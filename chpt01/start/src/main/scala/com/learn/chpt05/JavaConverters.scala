package com.learn.chpt05

import java.time.LocalDateTime
import java.util
import scala.jdk.CollectionConverters._

object JavaConverters extends App {
  val javaList = new util.ArrayList[LocalDateTime]()
  javaList.add(LocalDateTime.now())
  javaList.add(javaList.get(0).plusDays(1))

  println(s"Times: $javaList")

  val scalaDates = javaList.asScala
  scalaDates.foreach { date=> println(s"Date: $date")}

  val backToBlack = scalaDates.asJavaCollection
  println(backToBlack)

}
