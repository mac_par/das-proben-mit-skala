package com.learn.chpt05

object RestFullApi extends App {
  val listOfMethods = List(("GET", "/user/:id"),
    ("GET", "user/:id/profile/:p_id"),
    ("POST", "/user"),
    ("POST", "/profile"),
    ("PUT", "/user/:id"))

  //immutable map(String,List(Tuplet(String,String))))
  val groupedRestApiMap = listOfMethods.groupBy(_._1)
  println(s"grouped Api methods: $groupedRestApiMap")

  //immutable map(String, List(String))
  val apisByMethod = groupedRestApiMap.mapValues[List[String]](_.map(_._2))
  println(s"apis by methods: $apisByMethod")
}
