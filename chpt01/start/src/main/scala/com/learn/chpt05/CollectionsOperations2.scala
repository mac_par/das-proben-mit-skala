package com.learn.chpt05

import java.util.Objects

import scala.io.Source

object CollectionsOperations2 extends App {
  def getPlayers(fileName: String): List[Player] = {
    getCsvList(fileName) match {
      case head :: tail => tail.map[Player] { line =>
        val columns = line.split(",").map(_.trim)
        Player(columns(5), columns(6), columns(9), columns(7),
          columns(8), columns(10), columns(12), columns(0), columns(2))
      }
      case Nil => List[Player]()
    }
  }

  def getCsvList(fileName: String): List[String] = {
    var source: Source = null
    try {
      source = io.Source.fromInputStream(getClass.getResourceAsStream(fileName))
      source.getLines().toList
    } catch {
      case e: Exception => throw e
    } finally {
      if (!Objects.isNull(source)) {
        source.close()
      }
    }
  }

  def showPlayer(player: Player) = {
    println(
      s"""Player: ${player.name}\tCountry: ${player.nationality}\tRanking 2016: ${player.ranking2016}

      ***** Other Information *****
      Age: ${player.age}| Club: ${player.club}| Domestic League: ${player.domesticLeague}
      Raw Total: ${player.rawTotal}| Final Score: ${player.finalScore}| Ranking 2015: ${player.ranking2015}
      ##########################################################""")
  }

  val players = getPlayers("/football_stats.csv")

  //z pierwszej 50tki wyznacz 20 i 30stke 2016
  val top50 = players take 50
  val (top20, top30) = top50.partition(_.ranking2016.toInt < 21)
  //part
  top20.foreach(showPlayer)
}


//case class Player(name: String, nationality: String, age: String, club: String,
//                  domesticLeague: String, rawTotal: String, finalScore: String, ranking2016: String,
//                  ranking2015: String)
