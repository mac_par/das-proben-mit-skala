package com.learn.chpt05

import scala.collection.immutable.TreeSet

object TreeSetImpl extends App {
  implicit val ordering = Ordering.fromLessThan[Int](_ > _)
  val tree = new TreeSet() + (1,3,12,3,5)
  println(tree)
}
