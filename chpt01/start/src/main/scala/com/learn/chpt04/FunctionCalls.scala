package com.learn.chpt04

object FunctionCalls extends App {
  def average(number:Int*): Double = number.foldLeft(0)((a, b) => a+b) / number.length
  def averageV1(numbers: Int*):Double = numbers.sum / numbers.length

  println(average(2,2))
  println(average(1,2,3))
  println(averageV1(1,2,3))
}
