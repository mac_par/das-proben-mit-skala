package com.learn.chpt04

object FunctionSyntax1 extends App {

  def compareIntVal(val1: Int, val2: Int) = {
    val compareVal = val1.compareTo(val2)
    giveaAMeaningfulResult(compareVal, val1, val2)
  }

  private def giveaAMeaningfulResult(result: Int, value1: Int, value2: Int) = result match {
    case -1 => s"$value1 is smaller that $value2"
    case 0 => "Values are equal"
    case 1 => s"$value1 is greater that $value2"
    case _ => "Fuck! Weak the fuck up"
  }

  println(compareIntVal(2, 1))
}
