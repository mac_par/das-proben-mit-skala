package com.learn.chpt04

object CompareIntsV2 extends App {

  def compareTo(val1: Int, val2: Int = 10)  = {
    println(s"Porownywanie [$val1,$val2]")

    def resultToString(result: (Int, Int) => Int) = result.apply(val1,val2) match {
      case -1 => s"$val1 is smaller than $val2"
      case 0 => "Values are equal"
      case 1 => s"$val1 is greater than $val2"
      case _ => "What the fuck had happended"
    }

    val result = (a: Int, b: Int) => a.compareTo(b)
    resultToString(result)
  }

  println(compareTo(1, 2))
  println(compareTo(2, 1))
  println(compareTo(2, 2))
  println(compareTo(2))
  println(compareTo(val2 = 5, val1 = 3))
}
