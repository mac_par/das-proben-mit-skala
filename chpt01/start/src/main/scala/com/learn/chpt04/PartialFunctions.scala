package com.learn.chpt04

object PartialFunctions extends App {
  val isPrimeEligible: PartialFunction[Item,Boolean] = {
    case item => item.isPrimeEligible
  }

  val amountMoreThan500: PartialFunction[Item,Boolean] = {
    case item => item.price > 500.0
  }

  val freeDeliverable = isPrimeEligible orElse amountMoreThan500

  def deliveryCharges(item: Item): Double = if(freeDeliverable(item)) 0 else 500

  println(deliveryCharges(Item("1","ABC Keyboard",490.0, false)))
  println(deliveryCharges(Item("1","ABC Keyboard",500.0, false)))
  println(deliveryCharges(Item("1","ABC Keyboard", 300.0, true)))
}

case class Item(id: String, name: String, price: Double, isPrimeEligible: Boolean)
