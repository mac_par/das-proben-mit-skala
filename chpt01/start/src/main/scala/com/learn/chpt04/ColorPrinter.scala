package com.learn.chpt04

object ColorPrinter extends App {
  val printerSwitch =false
  def printPages(doc: Document, lastIndex: Int, print: Int => Unit,isPrinterOn: => Boolean) = {
    if (lastIndex<= doc.numPages && isPrinterOn) for (i <- 1 to lastIndex) print(i)
  }

  val colorPrint = (index: Int) => {
    println(s"Printing page $index in color")
  }

  printPages(Document(15, "DOCX"), 5, colorPrint, !printerSwitch)
}

case class Document(numPages: Int, typeOfDoc: String);
