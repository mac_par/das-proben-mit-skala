package com.learn.chpt01

object Start02 extends App {
    val double : (Int => Int) = _ *2
  (1 to 10) foreach double.andThen(println)
}
