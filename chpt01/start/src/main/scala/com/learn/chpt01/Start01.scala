package com.learn.chpt01

object Start01 {
  def main(args: Array[String]): Unit = {
    val double : Int => Int = _ *2
    (1 to 10 ) foreach double.andThen(println)
  }
}
