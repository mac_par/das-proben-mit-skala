package com.learn.chpt01

import java.util.UUID

class Item{ val id: UUID = UUID.randomUUID()}

class ElectronicItem(val name: String, val subCategory: String) extends Item {
  val uuid :String = "Elect"+id;
}

object CartApp extends App{
  def showItem(item: ElectronicItem) = println(s"Item: ${item.id}, UUID: ${item.uuid}, name: ${item.name}")
  def recursiveFactorial(x: Int): Int = if (x == 0) 1 else recursiveFactorial(x-1)

  this.showItem(new ElectronicItem("Xperia", "Mobile"))
  showItem(new ElectronicItem("iPhone", "mobile"))
}
