package com.learn.chpt01

class Amount(val amt: Double) {
  def taxApplied(tax: Double) = this.amt * tax/100 + this.amt
}

object Order  extends App {
  val tax = 10
  val orderValue = 130

  def amountAfterTax(amount: Amount) = amount taxApplied tax
  println(s"Total amount after tax ${amountAfterTax(new Amount(orderValue))}")
}
