package com.learn.chpt06.tratus

class Employee extends Person {
  override val category: String = "Internal"
  override val idPrefix: String = "EMP"
}
