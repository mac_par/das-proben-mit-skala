package com.learn.chpt06.tratus

trait Person {
  val category: String
  val idPrefix: String
}
