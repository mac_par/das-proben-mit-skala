package com.learn.chpt06.tratus

class Customer extends Person {
  override val category: String = "EXTERNAL"
  override val idPrefix: String = "CUST"
}
