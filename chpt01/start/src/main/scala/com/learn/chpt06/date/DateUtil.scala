package com.learn.chpt06.date
import java.time.{DayOfWeek, LocalDate}
import java.time.format.{DateTimeFormatter, TextStyle}
import java.util.Locale

import scala.util.{Failure, Success, Try}

object DateUtil {

  def dayOfWeek(string:String): Option[String] = Try {
    LocalDate.parse(string, DateTimeFormatter.ofPattern("dd/MM/yyyy")).getDayOfWeek
  } match {
    case Success(value) => Some(value.getDisplayName(TextStyle.FULL, Locale.ENGLISH))
    case Failure(e) => e.printStackTrace(); None
  }
}
