package com.learn.chpt06

object CountryApp extends App {
  val country = new Country("France", "Paris")
  country setPopulation  ("2015" -> 64.39)
  country setPopulation ("2016" -> 64.67)
  country setPopulation ("2017" -> 64.93)
  println(s"Country: ${country.name}, Capital: ${country.capital}, Population:" +
    s" ${country.getPopulation("2017")} millions")
  println(country)
  println(s"${country.name} with average population of " +
    s"${Country populationAverage country} millions")
}
