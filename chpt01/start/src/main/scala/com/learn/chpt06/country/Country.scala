package com.learn.chpt06.country

case class Country(name:String, capital: String)

object Country{
  def getDetails(country: Country): Unit = println(s"${country.name} - ${country.capital}")
}
