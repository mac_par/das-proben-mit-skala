package com.learn.chpt06.country

object CountryUtil extends App {
  val country = Country("France", "Paris")
  val country2 = Country("France", "Paris")
  println(country)
  Country.getDetails(country)

  println(s"Equality: ${country == country}")
  println(s"Equality: ${country == country2}")
  println(s"HashCode: Ctr ${country.hashCode()}")
  println(s"HashCode: Ctr2 ${country2.hashCode()}")
  println(s"Unapply: ${Country.unapply(country)}")
  println(s"Apply: ${Country.apply("Lebanon", "Beirut")}")
  println(s"Copy => ${country.copy("Germany", "Berlin")}")
  println(s"Copy name => ${country.copy(name = "Germany")}")
  println(s"Copy capital => ${country.copy(capital = "Berlin")}")

  println(s"ProductArity => ${country.productArity}")
  println(s"Product prefix => ${country.productPrefix}")
  println(s"Product element(0) => ${country.productElement(0)}")
  println(s"Product element(1) => ${country.productElement(1)}")

  val countries = Array(Country("Germany", "Berlin"), Country("Lebanon", "Beirut"), Country("France", "Paris"))
  for {
    country <- countries
  } {
    println(country)
    country match {
      case Country("Germany", _) => println("It's Germany")
      case Country("France",_) => println("It's France")
      case Country(_,_) => println("Unknown land")
    }
  }
}
