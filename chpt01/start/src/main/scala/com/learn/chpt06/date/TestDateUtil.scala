package com.learn.chpt06.date

object TestDateUtil extends App {
  import DateUtil._
  val date = "01/01/1992"
  dayOfWeek(date) match {
    case Some(janko) => println(s"It's $janko on $date")
    case None => println("Dupa blada!")
  }

  val result = Date(date)
  println(result)
}
