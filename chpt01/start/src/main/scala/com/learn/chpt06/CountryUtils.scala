package com.learn.chpt06

object CountryUtils {
  def populationAverage(country: Country): Double = {
    val seq = country.getPopulationSeq
    seq.sum / seq.length
  }
}
