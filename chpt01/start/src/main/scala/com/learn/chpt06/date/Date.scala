package com.learn.chpt06.date
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class Date(val dateStr:String) {
  override def toString: String = s"Date{$dateStr}"
}

object Date {
  def apply(str: String): Date = {
    val date = LocalDate.parse(str, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
    new Date(s"${date.getDayOfWeek} ${date.getDayOfMonth}-${date.getMonth}")
  }
}
