package com.learn.chpt06

class Country(val name:String, val capital: String) {
  private val populationMap = scala.collection.mutable.Map[String, Double]()
  protected val bajabongo: String = ""

  def getPopulation(year: String): Double  = populationMap(year)
  def setPopulation(entry: (String,Double)): Unit = populationMap += entry
  def getPopulationSeq:Seq[Double] = this.populationMap.values.toSeq

  override def toString: String = String.format(s"$name => $capital")
}

object Country {
  def populationAverage(country: Country): Double = {
    val seq = country.getPopulationSeq
    seq.sum / seq.length
  }
}