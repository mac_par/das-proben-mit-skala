package com.learn.chpt03
import scala.annotation.tailrec

object TailRecursionEx extends App{
  def power2ToNTailed(n: Int):Int = {
    @tailrec
    def helper(n:Int, currval: Int): Int = {
      if (n == 0) return currval else helper(n-1, currval * 2);
    }

    helper(n, 1)
  }

  println(power2ToNTailed(2))
  println(power2ToNTailed(4))
  println(power2ToNTailed(6))
}
