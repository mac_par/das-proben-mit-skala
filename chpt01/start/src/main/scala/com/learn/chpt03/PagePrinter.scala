package com.learn.chpt03

object PagePrinter extends App {
  def printPages(doc: Document, lastIdx: Int) = if (lastIdx <= doc.numberOfPages)
    for (i <- 1 to lastIdx) print(i)

  def printPages(doc: Document, startIdx: Int, lastIdx: Int) = if (startIdx > 0 && startIdx < lastIdx &&
    lastIdx <= doc.numberOfPages)
    for (i <- startIdx to lastIdx) print(i)

  /**
    * Strony z wybranymi indexami
    */

  def printPages(doc: Document, indexes: Int*) = for (i <- indexes if i > -1 && i <= doc.numberOfPages)
  print(i)

  private def print(idx: Int) = println(s"Printing page $idx")

  println("Method V1")
  printPages(Document(15, "DOCX"), 5)

  println("Method V2")
  printPages(Document(15, "DOCX"), 2,5)

  println("Method V3")
  printPages(Document(15, "DOCX"), 2,5,7,30,-1, 15)
}

case class Document(numberOfPages: Int, typeOfDoc: String)
