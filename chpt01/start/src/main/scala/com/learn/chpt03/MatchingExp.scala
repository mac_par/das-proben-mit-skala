package com.learn.chpt03

object MatchingExp extends App {

  def matchAgainst(n: Int) = n match {
    case 1 => println ("One")
    case 2 => println ("Two")
    case 3 => println ("Three")
    case 4 => println ("Four")
    case _ => println ("??")
  }

  matchAgainst(1)
  matchAgainst(2)
  matchAgainst(3)
  matchAgainst(4)
  matchAgainst(5)
  matchAgainst(6)
}
