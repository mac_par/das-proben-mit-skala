package com.learn.chpt03

object ForExpressions2 extends App {
  val person1 = Person("Albert", 21, 'm')
  val person2 = Person("Bob", 25, 'm')
  val person3 = Person("Cyril", 19, 'f')


  val persons = List(person1, person2, person3)

  val winners = for {
    person <- persons
    if person.age > 20
  } yield person.name

  winners.foreach(println)

  case class Person(name: String, age: Int, gender: Char)
}
