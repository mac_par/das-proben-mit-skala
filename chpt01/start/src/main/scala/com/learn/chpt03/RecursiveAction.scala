package com.learn.chpt03

object RecursiveAction extends App {
  def powerOf2N(n: Int): Int = if (n == 0) 1 else 2* powerOf2N(n-1)

  println(powerOf2N(2))
  println(powerOf2N(4))
  println(powerOf2N(6))
  println(powerOf2N(10))
}
