package com.learn.chpt07.exer1

class Dictionary(name: String) extends Book(name){
//  override def cover(cover: String): String = s"Hardcover_$cover"
  override val coverType: String = "Hardcover"
}
