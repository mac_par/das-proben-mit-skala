package com.learn.chpt07.exer2

import java.time.LocalDate

object StartUpApp extends App{
  val startUpWithFoundingDate = new StartUp("WSup", Employee("Rahul Sharma",
    "RH_ID_1", "9090000321", "rahul_sharma@abc.com"), LocalDate.now())
  println(s"""${startUpWithFoundingDate.name} founded on
    ${startUpWithFoundingDate.foundingDate.get} by
    ${startUpWithFoundingDate.founder.name}""")
  val startUp = new StartUp("Taken", Employee("David Barbara", "DB_ID_1",
    "9090654321", "david_b@abc.com"))
  println(s"${startUp.name} founded by ${startUp.founder.name}")


  val startup = StartUp("WSup", Employee("Rahul Sharma",
    "RH_ID_1", "9090000321", "rahul_sharma@abc.com"), LocalDate.now())
  println(s"${startup.name} founded by ${startup.founder.name}")
}
