package com.learn.chpt07.exer1

object BookApp extends App {
  val book = new Book("The new World order")
  book.addChapter(Chapter("Chapter1", 1, 15))
  book.addChapter(Chapter("Chapter2", 2, 25))
  book.addChapter(Chapter("Chapter3", 3, 30))

  println(book.title)
  println(book.pages)

  val dictionary = new Dictionary("Here and there")
  println(dictionary cover "Marko polo")
}
