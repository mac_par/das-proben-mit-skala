package com.learn.chpt07.exer3

trait Socialize {
  def greet(name: String) = s"Hello $name"
  protected val socialNetworks = Set("Facebook", "LinkedIn", "Twitter", "Instagram", "Youtube")
  def linkToSocialNetwork(network: String, uri: String)
}
