package com.learn.chpt07.exer3_mixin

trait CreditCardOps {
  self: CreditCard =>
  val ccNumber: String = ccType match {
    case "BASIC" => s"BC_$ccNum"
    case _ => s"DC_$ccNum"
  }

  val brum: String = self match {
    case p:CreditCard => "Ok"
    case _ => "O jeju"
  }
}
