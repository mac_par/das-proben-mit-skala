package com.learn.chpt07.exer2

import java.time.LocalDate

case class StartUp(name: String, founder: Employee,
                   coFounders: Option[Set[Employee]],
                   members: Option[List[Employee]],
                   foundingDate: Option[LocalDate]){

  //only name and founder
  def this(name: String, founder: Employee) = this(name, founder, None, None, None)

  //founder and founding date
  def this(name:String, founder:Employee, date: LocalDate) = this(name, founder, None, None, Some(date))

  //founder, cofounders
  def this(name: String, founder: Employee, coFounders: Set[Employee]) = this(name,
    founder, Some(coFounders), None,None)

  //founder, cofounders, members
  def this(name:String, founder: Employee, coFounders: Set[Employee], members: List[Employee]) =
    this(name, founder,Some(coFounders), Some(members), None)
}

object StartUp {
  def apply(name: String, founder: Employee): StartUp = new StartUp(name, founder)
  def apply(name: String, founder: Employee, foundationDate: LocalDate): StartUp =
    new StartUp(name, founder, foundationDate)
  def apply(name: String, founder: Employee, coFounders: Set[Employee]): StartUp =
    new StartUp(name, founder, coFounders)
  def apply(name:String, founder: Employee, coFounders: Set[Employee], members: List[Employee]) = new StartUp(
    name, founder,coFounders,members)
}
