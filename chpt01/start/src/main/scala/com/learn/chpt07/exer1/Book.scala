package com.learn.chpt07.exer1

class Book(val title: String) {
  private val chapters = scala.collection.mutable.Set[Chapter]()
  def addChapter(chapter: Chapter): Unit = chapters.add(chapter)
  def pages(): Int = chapters.foldLeft(0)((a, b)=> a+b.nbrOfPg)
  def cover(cover: String): String = s"${coverType}_$cover"
  def coverType: String = "Paperback"
}
