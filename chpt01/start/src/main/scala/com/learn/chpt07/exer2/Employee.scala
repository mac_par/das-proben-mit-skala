package com.learn.chpt07.exer2

case class Employee(name: String, id: String, contact: String, email: String = "mir@wp.pl") {

  def this(name: String) = this(name, null, null)
}

object Employee {
  def apply(name: String) = new Employee(name)
}
