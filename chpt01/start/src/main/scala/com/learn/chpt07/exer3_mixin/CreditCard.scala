package com.learn.chpt07.exer3_mixin

abstract class CreditCard {
  val ccType = "Default"
  def creditLimit(x: Double): Amount
  val ccNum: String = scala.util.Random.nextInt(1000000000).toString
}
