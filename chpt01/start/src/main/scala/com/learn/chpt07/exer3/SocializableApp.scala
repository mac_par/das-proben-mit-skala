package com.learn.chpt07.exer3


object SocializableApp extends App {
  val person = Person("Marko")

  val employee = new Employee("Barbra Streisand")
  employee.linkToSocialNetwork("LinkedIn", "linkedin.com")


  println(employee.greet(person.name))
//  println(employee.socialNetworks)
  println(employee.socialMap)
}
