package com.learn.chpt07.exer3_mixin

object CCApp extends App{
  val basicCreditCard = new CreditCard {
    override def creditLimit(x: Double): Amount = Amount(x, "USD")
  }

  val limit = basicCreditCard.creditLimit(100000.0d)

  println(s"Credit card ${basicCreditCard.ccNum} with limit of $limit")
}
