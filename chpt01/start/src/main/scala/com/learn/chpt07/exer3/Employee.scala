package com.learn.chpt07.exer3

class Employee(name: String) extends Person(name) with Socialize {
  override def linkToSocialNetwork(network: String, uri: String): Unit = if (socialNetworks contains network)
    socialMap += (network -> uri)
  val socialMap = scala.collection.mutable.HashMap[String,String]()
  override protected val socialNetworks: Set[String] = Set("LinkedIn", "Facebook", "Twitter")
}
