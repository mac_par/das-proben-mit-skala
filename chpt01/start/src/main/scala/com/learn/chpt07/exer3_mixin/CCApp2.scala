package com.learn.chpt07.exer3_mixin

object CCApp2 extends App{
  val basicCreditCard = new CreditCard with Limitable

  val limit = basicCreditCard.creditLimit(100000.0d)

  println(s"Credit card ${basicCreditCard.ccNum} with limit of $limit")
}
