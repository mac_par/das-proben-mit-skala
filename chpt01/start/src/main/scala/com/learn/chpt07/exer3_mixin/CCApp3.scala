package com.learn.chpt07.exer3_mixin

object CCApp3 extends App{
  val basicCreditCard = new CreditCard with CreditCardOps {
    override def creditLimit(x: Double): Amount = Amount(x, "USD")
  }

  val limit = basicCreditCard.creditLimit(100000.0d)

  println(s"Credit card ${basicCreditCard.ccNum} with limit of $limit of number ${basicCreditCard.ccNumber} ${basicCreditCard.brum}")
}
