package com.learn.chpt07.exer3_mixin

case class Amount(amt: Double, currency: String) {
  override def toString: String = f"$amt%.2f ${currency.toUpperCase}%s"
}
