package com.learn.chpt07.exer3_mixin

trait Limitable {
  def creditLimit(x: Double): Amount = Amount(x, "USD")
}
